### Philosophy
---
- Runs in Vanilla Doom, with enhancements possible in other source ports as the "cherry on top".
- Amplify Doom's horror undertones while not being too serious.
- Contain subtle visual story telling.

### The Story So Far
---
The events of CABIN occur directly after DOOM II, serving as an alternate scenario if our hero returned home to Earth instead of staying in Hell.

After you defeated the Icon of Sin, a portal back home to Earth was opened. Seeing the portal, you thought to yourself: How long as it been since the UAC teleporter incident? Days, weeks, months? Having trouble remembering, you came to the conclusion that you needed a break, so you became immersed with the aura of the teleporter as Hell melted and faded away into Earth's green foliage. Home at last. You're finally safe, joined with Earth's population that you rescued.

You're revered as not only the savior of humanity, but Earth itself. Your name is in the history books and you have statues across the globe, your birthday is celebrated as an international holiday, and cults were even started about you. However you couldn't be any less concerned about your fame. Your experience has left you with some mental scarring, haunted by the screams of the damned, the faces of the innocent that you couldn't save, and the best years of your life forever lost to the sands of time.

With that, you decided that living in the deep and quiet seclusion of the woods was the best course of action. With the chainsaw, you built yourself a cozy little cabin. You've gotten rid of the souvenirs that reminded you of your traumatizing past, including your original weapons. Finally, peace and serenity. But you never really lived in total isolation, as some of the most thankful people quietly left you food and gifts at your doorstep occasionally. 

So many years passed by, until one late October night you're woken up by frantic screaming and scratching outside your front door. After grabbing your M1911 off your nightstand, the noises were ended by a sickening splatter. Silence permeated the air once again, but it's a heavy silence. Not even the chirping of crickets filled the silence. If this was a prank, it wouldn't have felt this off.

While looking for your key, a grim realization hit you: your key was misplaced from the usual spot you always kept it. Has reality distorted around you? Eerily, this was all too familiar to the effect Hell had. Whatever form Hell is now, you're gonna put an end to it for good. One last time.

### Notes
---
- 7 new maps.
- Uses DEHACKED. (ZScript is used instead for max mod compatibility when running GZDoom!)
	- Unique weapons.
	- Unique enemies.
	- Unique decorations.

### Build instructions
---
- [Compile CABIN_decohack.dh with DECOHACK from DoomTools.](https://github.com/MTrop/DoomTools) After doing that, change the file extension of `dehacked.deh` to `dehacked.lmp`.
- [Build the repository with DeuTex.](https://github.com/Doom-Utils/deutex) For example, to build with Windows PowerShell, enter `./deutex -doom2 c:/doom -s_end -build cabininfo.txt CABIN.wad`. The outputted .WAD file works out of the box with Chocolate Doom (using -dehlump), PrBoom+ and GZDoom.

### How to run
---
- Chocolate Doom / Crispy Doom
	- Run with `-file CABIN.wad -dehlump CABIN.wad`. Or do `-file CABIN.wad -deh CABIN.deh` for the loose DEHACKED.
- Other source ports (GZDoom, PrBoom)
	- Only load `CABIN.wad`. Typically, source ports will automatically load the DEHACKED built into the .WAD file.

### Mod compatibility
---
Load `CABIN.wad` *LAST*!
- Not compatible with other DEHACKED mods. This is because it's DEHACKED. Seriously dude, don't try it.
- Somewhat compatible with other GZDoom mods. Effort was done to ensure it's beatable (eg: the final boss won't softlock you).
- Zandrolol may not play nice if it's loaded with other mods.

### Credits
---
Author / Designer:
- UndeadRyker (Maps, DEHACKED, basic ZScript and GZDoom support, and everything else)

Musician:
- James "Jimmy" Paddock (Music, duh)

Artists:
- Verlion D'Gorche (TITLEPIC, logo)
- 00_Zombie_00 ("Naked" Revenant)
- Ultra64 (HUD Face)
- Vader (Revenant projectile)
- Gothic (Final boss)
- MagicWazard (Final boss)
- JoeyTD (M1911)
- Xaser (Roots)
- Captain Awesome (Roots)
- Skulltag (Minigun)
- 3D Realms (M1911, Roots)

Special Thanks:
- Chaki Kobayashi (Source photo for RSKY1)
- Gregor Punchatz (Revenant design, source photo for INTERPIC)

### Known Bugs
---
- [Completing MAP01 may cause player 1 to start MAP02 as a zombie.](https://doomwiki.org/wiki/Voodoo_doll#Technical) Seems to be very rare. Only happened to me once.