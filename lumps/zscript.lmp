version "4.8.2"

class URCForsakenCutscene : URCForsaken replaces Fatso { Default { +BUDDHA -COUNTKILL } }
class URCForsaken : Actor replaces Revenant
{
	Default
	{
		Health 35;
		Radius 20;
		Height 56;
		Mass 300;
		Speed 7;
		PainChance 0;
		BloodColor "79443b";
		Monster;
		MeleeThreshold 196;
		+MISSILEMORE
		+FLOORCLIP
		SeeSound "skeleton/sight";
		PainSound "skeleton/pain";
		DeathSound "skeleton/death";
		ActiveSound "skeleton/active";
		HitObituary "$OB_URCFORSAKENHIT";
		Obituary "$OB_URCFORSAKEN";
		Tag "$FN_URCFORSAKEN";
	}
	States
	{
		Spawn:
			URUN AB 10 A_Look;
			Loop;
		See:
			URUN AAABBBCCCDDDEEEFFF 1 A_Chase;
			Loop;
		Melee:
			URUN G 0 A_StartSound("skeleton/swing", CHAN_WEAPON);
			URUN H 2 A_FaceTarget;
			URUN H 0 A_CustomMeleeAttack(random(1, 10) * 6, "skeleton/melee");
			URUN I 17 A_FaceTarget;
			Goto See;
		Missile:
			URUN G 13 A_FaceTarget;
			URUN L 13 A_SpawnProjectile("URCForsakenVomit");
			URUN L 7 A_FaceTarget;
			Goto See;
		Pain:
			URUN L 3;
			URUN L 5 A_Pain;
			Goto See;
		Death:
			URUN LM 7;
			URUN N 7 A_Scream;
			URUN O 7 A_Fall;
			URUN P 7;
			URUN Q -1;
			Stop;
	}
}

class URCForsakenVomit : Actor
{
	Default
	{
		Radius 11;
		Height 8;
		Speed 30;
		FastSpeed 60;
		Damage 10;
		Projectile;
		+RANDOMIZE
		+ZDOOMTRANS
		RenderStyle "Add";
		Alpha 1;
		SeeSound "skeleton/attack";
		DeathSound "skeleton/tracex";
		Decal "BaronScorch";
	}
	States
	{
		Spawn:
			URUM AB 4 Bright;
			Loop;
		Death:
			URME A 8 Bright;
			URME B 6 Bright;
			URME C 4 Bright;
			Stop;
	}
}

class URCAlastor : Actor replaces DoomImp
{
	Default
	{
		Health 4500;
		Radius 20;
		Height 56;
		Mass 6666;
		Speed 8;
		PainChance 6;
		BloodColor "000000";
		Monster;
		+BOSS
		+FLOAT
		+NOGRAVITY
		PainSound "brain/pain";
		DeathSound "brain/death";
		ActiveSound "fatso/raiseguns";
		HitObituary "$OB_URCALASTORHIT";
		Obituary "$OB_URCALASTOR";
		Tag "$FN_URCALASTOR";
	}
	States
	{
		Spawn:
			URDW A 1 A_Look;
			Loop;
		See:
			URDW A 1 A_Chase;
			Loop;
		Melee:
		Missile:
			URDW B 3 A_FaceTarget;
			URDW C 3 Bright A_FaceTarget;
			URDW B 3 A_FaceTarget;
			URDW C 3 Bright A_FaceTarget;
			URDW D 8 Bright A_URCAlastorAttack;
			Goto See;
		Pain:
			URDW E 13 A_StartSound("brain/pain", CHAN_VOICE, CHANF_DEFAULT, 1.0, ATTN_NONE);
			Goto See;
		Death:
			URDW E 10 Bright;
			URDW F 10 Bright A_StartSound("brain/death", CHAN_VOICE, CHANF_DEFAULT, 1.0, ATTN_NONE);
			URDW G 10 Bright;
			URDW H 10 Bright;
			URDW I 10 Bright;
			URDW J 10 Bright;
			URDW K 10 Bright;
			URDW L 10;
			URDW N 150;
			URDW N 5 A_BrainDie;
			Stop;
	}
}

extend class URCAlastor // [Ryker] This is pretty much the Baron attack action function, adjusted to fire its own missile for mod compatibility.
{
	void A_URCAlastorAttack()
	{
		if (target)
		{
			if (CheckMeleeRange())
			{
				int damage = random[pr_bruisattack](1, 8) * 10;
				A_StartSound ("baron/melee", CHAN_WEAPON);
				int newdam = target.DamageMobj (self, self, damage, "Melee");
				target.TraceBleed (newdam > 0 ? newdam : damage, self);
			}
			else { SpawnMissile (target, "URCAlastorBall"); }
		}
	}
}

class URCAlastorBall : BaronBall
{
	Default
	{
		Height 8;
		Speed 60;
		FastSpeed 120;
		Damage 20;
		Translation 2;
	}
}

class URCAlastorEyes : Actor replaces LostSoul // [Ryker] This is responsible for raising the door to fight Alastor.
{
	Default
	{
		Height 56;
		Radius 16;
		+NOBLOCKMAP
	}
	States
	{
		Spawn:
			TNT1 A 5 A_Look;
			Loop;
		See:
			TNT1 A 1 A_StartSound("brain/sight", CHAN_VOICE, CHANF_DEFAULT, 1.0, ATTN_NONE);
			TNT1 A 140;
			TNT1 A 1 A_KeenDie;
			Stop;
	}
}

class URCDeadForsaken : URCForsaken replaces GibbedMarineExtra
{
	Default
	{
		Skip_Super;
	}
	States
	{
		Spawn:
			Goto Super::Death+5;
	}
}

class URCDoorBanger : Actor replaces Archvile // [Ryker] Its sole purpose is to generate the "door bang" noise in OOB areas.
{
	Default
	{
		Radius 20;
		Height 56;
		Speed 1;
		SeeSound "vile/active";
		ActiveSound "vile/active";
	}
	States
	{
		Spawn:
			TNT1 A 5 A_Look;
			Loop;
		See:
			TNT1 A 5 A_Chase;
			Loop;
	}
}

class URCDecoCeil : URCDeco { Default { +SPAWNCEILING +NOGRAVITY } }
class URCDeco : Actor
{
	Default
	{
		Radius 20;
		Height 1;
		+NOBLOCKMAP
		+MOVEWITHSECTOR
	}
}

class URCRootCeilingTiny : URCDecoCeil replaces DeadDemon
{
	Default
	{
		Height 30;
	}
	States
	{
		Spawn:
			ROOT A -1;
			Stop;
	}
}

class URCRootFloorDouble : URCDeco replaces DeadDoomImp
{
	States
	{
		Spawn:
			ROOT B -1;
			Stop;
	}
}

class URCRootCeilingDouble : URCDecoCeil replaces NonsolidTwitch
{
	Default
	{
		Height 47;
	}
	States
	{
		Spawn:
			ROOT C -1;
			Stop;
	}
}

class URCRootCeilingSmall : URCDecoCeil replaces DeadLostSoul
{
	Default
	{
		Height 47;
	}
	States
	{
		Spawn:
			ROOT D -1;
			Stop;
	}
}

class URCRootCeilingBig : URCDecoCeil replaces DeadCacodemon
{
	Default
	{
		Height 84;
	}
	States
	{
		Spawn:
			ROOT E -1;
			Stop;
	}
}